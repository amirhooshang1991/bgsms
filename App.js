import React from 'react';
import {
  AsyncStorage,
  Button,
  View,
  PermissionsAndroid,
  AppState,
  Text,
} from 'react-native';
import LaunchApplication from 'react-native-bring-foreground';

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import SmsListener from 'react-native-android-sms-listener';
async function requestReadSmsPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.READ_SMS,
      {
        title: 'تایید خودکار کد فعال سازس',
        message:
          'ما برای تایید خودکار کد فعال سازی نیاز به دسترسی به پیامک های شما داریم',
      },
    );
    console.log(granted);
  } catch (err) {
    console.warn(err);
  }
}
class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.SMSReadSubscription = {};
    this.state = {
      appState: AppState.currentState,
    };
  }
  _handleAppStateChange = nextAppState => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      console.log('App has come to the foreground!');
    }
    this.setState({appState: nextAppState});
  };
  render() {
    return (
      <View>
        <Button
          title="Read results from AsyncStorage"
          onPress={async () => {
            this.props.navigation.navigate('Test');
            const result = await AsyncStorage.getItem('@MyApp:key');
            console.log(result);
          }}
        />
        <Text>Current state is: {this.state.appState}</Text>
      </View>
    );
  }

  async componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
    await requestReadSmsPermission();

    this.SMSReadSubscription = SmsListener.addListener(message => {
      console.log(this.state.appState);
      try {
        if (this.state.appState === 'background')
          LaunchApplication.open('com.rn61');
          this.props.navigation.navigate('Test');

        console.log('Message:', message);
      } catch (e) {
        console.log(e);
      }
    });
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
    this.SMSReadSubscription.remove();
  }
}
const Test = () => (
  <View>
    <Text>test</Text>
  </View>
);
const AppNavigator = createStackNavigator(
  {
    Home: HomeScreen,
    Test: Test,
  },
  {
    initialRouteName: 'Home',
  },
);

export default createAppContainer(AppNavigator);

// import React from 'react';
// import {
//   AsyncStorage,
//   Button,
//   View,
//   PermissionsAndroid,
//   AppState,
//   Text,
// } from 'react-native';

// import LaunchApplication from 'react-native-bring-foreground';

// import SmsListener from 'react-native-android-sms-listener';
// async function requestReadSmsPermission() {
//   try {
//     const granted = await PermissionsAndroid.request(
//       PermissionsAndroid.PERMISSIONS.READ_SMS,
//       {
//         title: 'تایید خودکار کد فعال سازس',
//         message:
//           'ما برای تایید خودکار کد فعال سازی نیاز به دسترسی به پیامک های شما داریم',
//       },
//     );
//     console.log(granted);
//   } catch (err) {
//     console.warn(err);
//   }
// }

// class MyApp extends React.Component {
//   constructor(props) {
//     super(props);
//     this.SMSReadSubscription = {};
//     this.state = {
//       appState: AppState.currentState,
//     };
//   }
//   _handleAppStateChange = nextAppState => {
//     if (
//       this.state.appState.match(/inactive|background/) &&
//       nextAppState === 'active'
//     ) {
//       console.log('App has come to the foreground!');
//     }
//     this.setState({appState: nextAppState});
//   };
//   render() {
//     return (
//       <View>
//         <Button
//           title="Read results from AsyncStorage"
//           onPress={async () => {
//             const result = await AsyncStorage.getItem('@MyApp:key');
//             console.log(result);
//           }}
//         />
//         <Text>Current state is: {this.state.appState}</Text>
//       </View>
//     );
//   }

//   async componentDidMount() {
//     AppState.addEventListener('change', this._handleAppStateChange);
//     await requestReadSmsPermission();

//     this.SMSReadSubscription = SmsListener.addListener(message => {
//       console.log(this.state.appState);
//       try {
//         if (this.state.appState === 'background')
//           LaunchApplication.open('com.rn61');

//         console.log('Message:', message);
//       } catch (e) {
//         console.log(e);
//       }
//     });
//   }

//   componentWillUnmount() {
//     AppState.removeEventListener('change', this._handleAppStateChange);
//     this.SMSReadSubscription.remove();
//   }
// }
// export default MyApp;
